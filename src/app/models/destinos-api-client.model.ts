import { DestinoViaje } from './destino-viaje.models';
import { Subject, BehaviorSubject } from 'rxjs';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient {
current: Subject<DestinoViaje>=new BehaviorSubject<DestinoViaje>(null);
	destinos:DestinoViaje[];
	constructor(){//private store: Store<AppState>) {
       this.destinos = [];
	}
	add(d:DestinoViaje){
		//this.store.dispatch(new NuevoDestinoAction(d));
	  this.destinos.push(d);
	}
	getAll(): DestinoViaje[]{
	  return this.destinos;
	}
	getById(id: String): DestinoViaje{
		return this.destinos.filter(function(d) {return d.id.toString()===id;})[0];
	}
	elegir(d: DestinoViaje){
		//this.store.dispatch(new ElegidoFavoritoAction(d));
		this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	}
	subscribeOnChange(fn){
		this.current.subscribe(fn);
	}
}