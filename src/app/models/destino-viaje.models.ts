import {v4 as uuid} from 'uuid';

export class DestinoViaje {
   public selected: boolean;
  servicios: string[];
  id = uuid();
  constructor(public nombre: string, public imagenUrl: string, public votes: number=0) {
       this.servicios = ['pileta', 'desayuno'];
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected() {
    return this.selected;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}